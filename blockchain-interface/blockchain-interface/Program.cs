﻿using System;
using System.Threading.Tasks;
using Nethereum.Web3;




// See https://aka.ms/new-console-template for more information
// Console.WriteLine("Hello, World!");

/*
 * This Project is a c# connection to the BBSE pruvate Ethereum chain. IT will
 * be used for the research project of AAA functionallities.
 * 
 * Tutorials: 
 * Installieren von Paketen mit NuGet: 
 * https://docs.microsoft.com/de-de/visualstudio/mac/nuget-walkthrough?toc=%2Fnuget%2Ftoc.json&view=vsmac-2019
 * 
 * Using Nethereum for connecting to bbse-Chain via C#
 * https://www.quicknode.com/guides/web3-sdks/how-to-connect-to-ethereum-using-net-nethereum
 * 
 * 
 */

namespace bbseconnection
{
    class Program
    {
        static void Main(string[] args)
        {
            GetBlockNumber().Wait();
        }

        static async Task GetBlockNumber()
        {
            var web3 = new Web3("http://194.163.134.215:9001");
            var latestBlockNumber = await web3.Eth.Blocks.GetBlockNumber.SendRequestAsync();
            var currentCoinbase = await web3.Eth.CoinBase.SendRequestAsync();
            Console.WriteLine($"Latest Block number is: {latestBlockNumber}, coinbase is: {currentCoinbase}");
        }
    }
}